// This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
// Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
// session persistence, api calls, and more.
const Alexa = require("ask-sdk-core");

const axios = require("axios");

let dataMem = {};

const { skillName } = require("./skillInfo");

const defaultImage =
  "https://linear-skill-generator.s3.amazonaws.com/default.png";

const getData = id => {
  return new Promise(resolve => {
    if (Object.keys(dataMem).length > 0) {
      resolve(dataMem);
    }

    const url = "https://linear-skill-generator.s3.amazonaws.com/scripts.json";

    axios.get(url).then(response => {
      const { data } = response;

      const searchId = data[id] ? id : "default";
      console.log({ skillName: id, searchId, data: data[searchId] });
      dataMem = data[searchId];
      resolve(data[searchId]);
    });
  });
};

// returns true if the skill is running on a device with a display (show|spot)
const supportsDisplay = handlerInput => {
  var hasDisplay =
    handlerInput.requestEnvelope.context &&
    handlerInput.requestEnvelope.context.System &&
    handlerInput.requestEnvelope.context.System.device &&
    handlerInput.requestEnvelope.context.System.device.supportedInterfaces &&
    handlerInput.requestEnvelope.context.System.device.supportedInterfaces
      .Display;
  return hasDisplay;
};

const isRoundDisplay = handlerInput => {
  const deviceShape = handlerInput.requestEnvelope.context.Viewport.shape;
  // assuming rectangle if undefined
  if (!deviceShape) {
    return false;
  }
  return deviceShape !== "RECTANGLE";
};

let index;

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === "LaunchRequest"
    );
  },
  async handle(handlerInput) {
    // TODO: if there is no skillId available get default.
    const data = await getData(skillName);
    const { responses, reprompts } = data;
    console.log({ responses, reprompts });
    index = 0;

    const images = isRoundDisplay(handlerInput)
      ? data.roundImages
      : data.rectangularImages;

    const response = handlerInput.responseBuilder;
    if (supportsDisplay(handlerInput)) {
      const imageUrl = images && images[0] ? images[0] : defaultImage;
      const backgroundImage = new Alexa.ImageHelper()
        .addImageInstance(imageUrl)
        .getImage();
      response.addRenderTemplateDirective({
        type: "BodyTemplate1",
        token: `Image ${index}`,
        backButton: "hidden",
        backgroundImage
      });
    }

    return response
      .speak(responses[0])
      .reprompt(reprompts[0])
      .getResponse();
  }
};

// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler = {
  canHandle() {
    return true;
  },
  async handle(handlerInput, error) {
    const data = await getData(skillName);
    const { responses, reprompts } = data;
    index += 1;

    const images = isRoundDisplay(handlerInput)
      ? data.roundImages
      : data.rectangularImages;

    const response = handlerInput.responseBuilder;
    if (supportsDisplay(handlerInput)) {
      const imageUrl = images && images[index] ? images[index] : defaultImage;
      const backgroundImage = new Alexa.ImageHelper()
        .addImageInstance(imageUrl)
        .getImage();
      response.addRenderTemplateDirective({
        type: "BodyTemplate1",
        token: `Image ${index}`,
        backButton: "hidden",
        backgroundImage
      });
    }

    return response
      .speak(responses[index])
      .reprompt(reprompts[index])
      .getResponse();
  }
};

// The SkillBuilder acts as the entry point for your skill, routing all request and response
// payloads to the handlers above. Make sure any new handlers or interceptors you've
// defined are included below. The order matters - they're processed top to bottom.

const handler = Alexa.SkillBuilders.custom()
  .addRequestHandlers(LaunchRequestHandler)
  .addErrorHandlers(ErrorHandler)
  .lambda();

module.exports = { handler };
