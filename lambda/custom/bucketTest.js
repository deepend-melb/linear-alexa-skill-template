const axios = require("axios");

const getData = (skillId) => {
  return new Promise(resolve => {
    const url = "https://linear-skill-generator.s3.amazonaws.com/scripts.json";

    axios.get(url).then(response => {
      const { data } = response;
      resolve(data[skillId]);
    });
  });
};

async function init() {
  const data = await getData('default');
  const {responses, reprompts} = data;

  console.log(data)
  console.log(responses, reprompts);

}

init();
